//
//  OVSettingsViewController.m
//  OVGUI
//
//  Created by Yu-Cheng Kao on 7/15/13.
//  Copyright (c) 2013 Daniel Kao. All rights reserved.
//

#import "OVSettingsViewController.h"

@implementation OVSettingsViewController

- (NSString *)portsPlistPath {
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    return [bundlePath stringByAppendingPathComponent:@"ports.plist"];
}

- (void)windowDidBecomeKey:(NSNotification *)notification {
    NSArray *ports = [NSArray arrayWithContentsOfFile:[self portsPlistPath]];
    self.port1TextField.stringValue = ports[0];
}

- (IBAction)save:(id)sender {
    
    NSMutableArray *ports = [NSMutableArray array];
    [ports addObject:self.port1TextField.stringValue];
    
    [ports writeToFile:[self portsPlistPath] atomically:YES];
    
    [self.settingsWindow orderOut:self];
    
}
@end
