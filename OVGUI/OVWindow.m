//
//  OVWindow.m
//  OVGUI
//
//  Created by Daniel on 10/23/13.
//  Copyright (c) 2013 Daniel Kao. All rights reserved.
//

#import "OVWindow.h"

@implementation OVWindow
- (id)initWithContentRect:(NSRect)contentRect styleMask:(NSUInteger)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag {
	self = [super initWithContentRect:contentRect styleMask:aStyle backing:bufferingType defer:flag];
	if (self) {
		self.styleMask = NSBorderlessWindowMask;
	}
	return self;
}
@end
