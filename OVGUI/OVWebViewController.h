//
//  OVWebViewController.h
//  OVGUI
//
//  Created by Daniel on 8/11/13.
//  Copyright (c) 2013 Daniel Kao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@interface OVWebViewController : NSWindowController <NSWindowDelegate>
@property (weak) IBOutlet NSView *view;
@property (unsafe_unretained) IBOutlet NSWindow *webWindow;
@property (weak) IBOutlet WebView *webView2;
@property (weak) IBOutlet WebView *webView;

@property (nonatomic, strong) NSString *location1;
@property (nonatomic, strong) NSString *location2;

@property (nonatomic, strong) NSString *locationDateString1;
@property (nonatomic, strong) NSString *locationDateString2;

@property (nonatomic, strong) NSMutableDictionary *activationDurationDic;

- (void)prepareWebView;
- (void)loadWeb;

- (void)startAnimation;
- (void)stopAnimation;
- (void)stopAnimation1;
- (void)stopAnimation2;

@end
