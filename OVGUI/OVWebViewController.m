//
//  OVWebViewController.m
//  OVGUI
//
//  Created by Daniel on 8/11/13.
//  Copyright (c) 2013 Daniel Kao. All rights reserved.
//

#import "OVWebViewController.h"

@interface OVWebViewController ()
@property (nonatomic, strong) NSURLRequest *request1;
@property (nonatomic, strong) NSURLRequest *request2;
@end

@implementation OVWebViewController

- (void)prepareWebView {
	
	[_webView setEditingDelegate:self];
	[_webView setUIDelegate:self];
	
	[[[_webView mainFrame] frameView] setAllowsScrolling:NO];
	[[[_webView2 mainFrame] frameView] setAllowsScrolling:NO];

	[[[[[_webView mainFrame] frameView] documentView] superview] scaleUnitSquareToSize:NSMakeSize(0.95, 0.95)];
	[[[[[_webView2 mainFrame] frameView] documentView] superview] scaleUnitSquareToSize:NSMakeSize(0.95, 0.95)];
	
	NSURL *url1 = 	[[NSURL alloc] initWithString:[kMagicURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *url2 = [[NSURL alloc] initWithString:[kMagicURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	
	_request1 = [NSURLRequest requestWithURL:url1];
	_request2 = [NSURLRequest requestWithURL:url2];
    
    [[_webView mainFrame] loadRequest:_request1];
    [[_webView2 mainFrame] loadRequest:_request2];
}

- (void)loadWeb {
    
    NSString *parameter1 = [NSString stringWithFormat:@"'%@', '%@'", _location1, _locationDateString1];
    NSString *parameter2 = [NSString stringWithFormat:@"'%@', '%@'", _location2, _locationDateString2];
    
    [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"magic.draw(%@)", parameter1]];
	[_webView2 stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"magic.draw(%@)", parameter2]];
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame {
	NSLog(@"%@", sender);
	NSLog(@"%@", frame);
	
//	[[[[[sender mainFrame] frameView] documentView] superview] scaleUnitSquareToSize:NSMakeSize(0.7, 0.7
//																								)];
}

- (void)startAnimation {
	// start('2013-10-11', '蘭嶼', '塔塔加', 蘭嶼時間, 塔塔加時間);
	NSString *jsString = [NSString stringWithFormat:@"start('%@', '%@', '%@', %@, %@)", _locationDateString1, _location1, _location2, _activationDurationDic[@"0"], _activationDurationDic[@"1"]];
	
	NSLog(@"activationDurationDic: %@", _activationDurationDic);
	
	[_webView stringByEvaluatingJavaScriptFromString:jsString];
//	[_webView2 stringByEvaluatingJavaScriptFromString:@"magic.startAnimation()"];
}

- (void)stopAnimation1 {
	[_webView stringByEvaluatingJavaScriptFromString:@"stop(POSITION.LEFT)"];
}

- (void)stopAnimation2 {
//	[_webView2 stringByEvaluatingJavaScriptFromString:@"magic.stopAnimation()"];
	[_webView stringByEvaluatingJavaScriptFromString:@"stop(POSITION.RIGHT)"];
}

- (void)stopAnimation {
    [self stopAnimation1];
    [self stopAnimation2];
}

- (NSArray *)webView:(WebView *)sender contextMenuItemsForElement:(NSDictionary *)element
    defaultMenuItems:(NSArray *)defaultMenuItems
{
    // disable right-click context menu
    return nil;
}

- (BOOL)webView:(WebView *)webView shouldChangeSelectedDOMRange:(DOMRange *)currentRange
	 toDOMRange:(DOMRange *)proposedRange
	   affinity:(NSSelectionAffinity)selectionAffinity
 stillSelecting:(BOOL)flag
{
    // disable text selection
    return NO;
}

- (void)windowDidResize:(NSNotification *)notification {
	NSLog(@"%@",notification);
	CGRect windowFrame = self.webWindow.frame;
	[self.webView.mainFrame.frameView setFrameSize:CGSizeMake(windowFrame.size.width - 20.0, windowFrame.size.height - 20.0)];
	[self.webView layout];
}





@end
