//
//  OVLocationUV.h
//  OVGUI
//
//  Created by Daniel on 6/27/13.
//  Copyright (c) 2013 Daniel Kao. All rights reserved.
//

@interface OVLocationUV : NSObject
@property (assign) NSInteger UV;
@property (assign) NSInteger time;
@end
