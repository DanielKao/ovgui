//
//  OVAppDelegate.m
//  OVGUI
//
//  Created by Daniel on 6/19/13.
//  Copyright (c) 2013 Daniel Kao. All rights reserved.
//

#import "OVAppDelegate.h"
#import "OVLocationUV.h"
#import "OVSettingsViewController.h"
#import "OVWebViewController.h"

@implementation OVAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// Insert code here to initialize your application
	self.datePicker.delegate = self;
	self.startHourPicker.delegate = self;
	self.endHourPicker.delegate = self;
	[self.window setMovableByWindowBackground:YES];
	[self.webViewController.webWindow setStyleMask:self.webViewController.webWindow.styleMask | NSResizableWindowMask];
	[self.webViewController.webWindow setMovableByWindowBackground:YES];
    self.webViewController.webWindow.backgroundColor = [NSColor whiteColor];
    [self.webViewController.webWindow makeKeyAndOrderFront:self];
    [self.webViewController prepareWebView];
	[self.activateButton setEnabled:NO];

}

- (void)applicationWillBecomeActive:(NSNotification *)notification {
	[self.locationTableView setEnabled:NO];
	[self.locationTableView2 setEnabled:NO];
    [self fetchLocationList];
}


#pragma mark - NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [_locationList count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {   
    return _locationList[row][@"location"];
}

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row {
    
	if ([self.activateButton.title isEqualToString:@"強制停止"]) {
		return NO;
	}
	
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    self.datePicker.minDate = [dateFormatter dateFromString:_locationList[row][@"first_date"]];
    self.datePicker.maxDate = [dateFormatter dateFromString:_locationList[row][@"last_date"]];
    self.dateRangeLabel.stringValue = [NSString stringWithFormat:@"%@ ~ %@", [dateFormatter stringFromDate:self.datePicker.minDate], [dateFormatter stringFromDate:self.datePicker.maxDate]];
    
    return YES;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
	[self.activateButton setEnabled:NO];
//	NSString *selectedDate, *location;
//	
//	if ([notification.object isKindOfClass:[NSTableView class]]) {
//		NSTableView *locationTableView = notification.object;
//		//Fetch selected date & convert to date string yyyy-MM-dd e.g. 2013-06-11
//		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//		[dateFormatter setDateFormat:@"yyyy-MM-dd"];
//		selectedDate = [dateFormatter stringFromDate:_datePicker.dateValue];
//		//Fetch location information
//		location = _locationList[locationTableView.selectedRow][@"location"];
//	} else if ([notification.object isKindOfClass:[NSDatePicker class]]){
//		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//		[dateFormatter setDateFormat:@"yyyy-MM-dd"];
//		selectedDate = [dateFormatter stringFromDate:_datePicker.dateValue];
//	}
//	
//	
//	if (notification.object == self.locationTableView) {
//		[self fetchWeatherInformationWithLocation:location andDate:selectedDate locationUVList:_location1UVList label:_oven1ActivationDurationLabel];
//	} else if (notification.object == self.locationTableView2) {
//		[self fetchWeatherInformationWithLocation:location andDate:selectedDate locationUVList:_location1UVList label:_oven1ActivationDurationLabel];
//	} else if (notification.object == self.datePicker){
//		
//		if (_locationTableView.selectedRow < 0 || _locationTableView.selectedRow >= _locationList.count || _locationTableView2.selectedRow < 0 || _locationTableView2.selectedRow >= _locationList.count) {
//			[self.activateButton setEnabled:NO];
//			return;
//		}
//
//		[self fetchWeatherInformationWithLocation:_locationList[_locationTableView.selectedRow][@"location"] andDate:selectedDate locationUVList:_location1UVList label:_oven1ActivationDurationLabel];
//		[self fetchWeatherInformationWithLocation:_locationList[_locationTableView2.selectedRow][@"location"] andDate:selectedDate locationUVList:_location2UVList label:_oven2ActivationDurationLabel];
//	}
//	
//	[self.activateButton setEnabled:NO];
}

- (void)datePickerCell:(NSDatePickerCell *)aDatePickerCell validateProposedDateValue:(NSDate **)proposedDateValue timeInterval:(NSTimeInterval *)proposedTimeInterval {
	[self.activateButton setEnabled:NO];
//	NSNotification *notification = [NSNotification notificationWithName:@"dateChanged" object:_datePicker];
//	[self tableViewSelectionDidChange:notification];
}

#pragma mark - IBActions

- (IBAction)calculateActivationDuration:(NSButton *)sender {
	
	[self.activateButton setEnabled:NO];
   
    if (_locationTableView.selectedRow == -1 || _locationTableView2.selectedRow == -1) {
        NSAlert *alert = [NSAlert alertWithMessageText:@"請選擇地點" defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:@"請檢查烤箱一與烤箱二的地點是否已經選擇"];
        [alert runModal];
        return;
    }
    
    
    //Fetch selected date & convert to date string yyyy-MM-dd e.g. 2013-06-11
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *selectedDate = [dateFormatter stringFromDate:_datePicker.dateValue];
    //Fetch location information
    
    NSString *location1 = _locationList[_locationTableView.selectedRow][@"location"];
    NSString *location2 = _locationList[_locationTableView2.selectedRow][@"location"];
	
	self.webViewController.location1 = location1;
	self.webViewController.location2 = location2;
	self.webViewController.locationDateString1 = selectedDate;
	self.webViewController.locationDateString2 = selectedDate;

    [self fetchWeatherInformationWithLocation:location1 andDate:selectedDate locationUVList:_location1UVList label:_oven1ActivationDurationLabel];
    [self fetchWeatherInformationWithLocation:location2 andDate:selectedDate locationUVList:_location2UVList label:_oven2ActivationDurationLabel];
    
//    [self.webViewController loadWeb];
}

- (IBAction)activateOven:(NSButton *)sender {
	
	NSString *filePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"ports.plist"];
	NSArray *ports = [NSArray arrayWithContentsOfFile:filePath];

	if ([ports[0] isEqualToString:@""]) {
		NSAlert *alert = [NSAlert alertWithMessageText:@"尚未輸入Arduino USB Port" defaultButton:@"確認" alternateButton:nil otherButton:nil informativeTextWithFormat:@"請輸入Arduino USB Port"];
		[alert runModal];
	} else {
		
//	    NSString *echoMsg1 = [NSString stringWithFormat:@"echo %ld@ > %@", (long)[_activationDurationList[0] integerValue], ports[0]];
//		NSString *echoMsg2 = [NSString stringWithFormat:@"echo %ld@ > %@", (long)[_activationDurationList[1] integerValue], ports[1]];
//		popen([echoMsg1 UTF8String], "r");
//		popen([echoMsg2 UTF8String], "r");

		NSString *echoMsgOn = [NSString stringWithFormat:@"echo n > %@", ports[0]];
//        NSString *echoMsgOn2 = [NSString stringWithFormat:@"echo on@ > %@", ports[1]];
        
        FILE *serialPort = popen([echoMsgOn UTF8String], "r");
		pclose(serialPort);
//        popen([echoMsgOn2 UTF8String], "r");

		self.oven1Timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateDurationLabel1) userInfo:nil repeats:YES];
		self.oven2Timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateDurationLabel2) userInfo:nil repeats:YES];
        
        [sender setTitle:@"強制停止"];
        [sender setAction:@selector(forceStopOven:)];
        [self toggleAllUI:[self.activateButton isEnabled]];
        [self.webViewController startAnimation];
	}
}

- (IBAction)forceStopOven:(id)sender {
   	NSString *filePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"ports.plist"];
	NSArray *ports = [NSArray arrayWithContentsOfFile:filePath];
    NSString *echoMsgForceOff = [NSString stringWithFormat:@"echo f > %@", ports[0]];
    FILE *serialPort = popen([echoMsgForceOff UTF8String], "r");
    pclose(serialPort);
    [self.oven1Timer invalidate];
    [self.oven2Timer invalidate];
    [self.webViewController stopAnimation];
    [sender setTitle:@"啟動烤箱"];
    [sender setAction:@selector(activateOven:)];
	[self toggleAllUI:!self.activateButton.isEnabled];
	
}

#pragma mark - Private Methods

- (void)toggleAllUI:(BOOL)isEnabled {
	for (NSView *view in [[self.window contentView] subviews]) {
		if ([view respondsToSelector:@selector(setEnabled:)]) {
			[(NSControl *)view setEnabled:!isEnabled];
		} else if ([view isKindOfClass:[NSView class]]) {
			[view setAcceptsTouchEvents:!isEnabled];
		}
	}
	[self.activateButton setEnabled:isEnabled];
}

- (void)fetchLocationList {
    __weak OVAppDelegate *weakSelf = self;
    NSURL *url = [NSURL URLWithString:kLocationListAPI];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        weakSelf.locationList = [NSArray arrayWithArray:(NSArray *)JSON];
        [weakSelf.locationTableView reloadData];
        [weakSelf.locationTableView2 reloadData];
		[self.locationTableView setEnabled:YES];
		[self.locationTableView2 setEnabled:YES];
    } failure:nil];
    
    [requestOperation start];
}

- (void)fetchWeatherInformationWithLocation:(NSString *)locationName andDate:(NSString *)dateString locationUVList:(NSMutableArray *)uvList label:(NSTextField *)durationLabel {
    
    self.activationDurationList = nil;
    self.activationDurationList = [NSMutableArray arrayWithCapacity:2];
    
    if (uvList == nil) {
        uvList = [NSMutableArray array];
    } else {
        [uvList removeAllObjects];
    }
    __weak OVAppDelegate *weakSelf = self;
    NSString *urlString = [NSString stringWithFormat:kLocationWeatherInformationAPI, locationName, dateString];
	NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																							   success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                                                                   NSInteger duration = 0;
                                                                                                   for (NSDictionary *dict in JSON) {
                                                                                                       OVLocationUV *locationUV = [[OVLocationUV alloc] init];
                                                                                                       locationUV.UV = [dict[@"uv"] integerValue];
                                                                                                       locationUV.time = [[[dict[@"hour"] componentsSeparatedByString:@" "] lastObject] integerValue];
                                                                                                       
                                                                                                       NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                                                                                       [dateFormatter setDateFormat:@"HH"];
                                                                                                       


                                                                                                       NSInteger startHour = [[dateFormatter stringFromDate:_startHourPicker.dateValue] integerValue];
                                                                                                       NSInteger endHour = [[dateFormatter stringFromDate:_endHourPicker.dateValue] integerValue];
                                                                                                       
                                                                                                       if (locationUV.time >= startHour && locationUV.time <= endHour) {
                                                                                                           [uvList addObject:locationUV];
                                                                                                           if (locationUV.UV <=2)
                                                                                                               duration += 0;
                                                                                                           else if (locationUV.UV <= 5)
                                                                                                               duration += 30;
                                                                                                           else if (locationUV.UV <=7)
                                                                                                               duration += 60;
                                                                                                           else if (locationUV.UV <= 10)
                                                                                                               duration += 90;
                                                                                                           else
                                                                                                               duration +=120;
                                                                                                       }
                                                                                                   }
                                                                                                   [self.activationDurationList addObject:@(duration)];
                                                                                                   [weakSelf setDurationLabel:durationLabel withDuration:duration];
                                                                                               } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
																								   
																							   }];
	[requestOperation start];
}

- (void)setDurationLabel:(NSTextField *)durationLabel withDuration:(NSInteger)duration {
	
	if (self.webViewController.activationDurationDic == nil) {
		self.webViewController.activationDurationDic = [NSMutableDictionary dictionaryWithCapacity:2];
	}
	
	[self.webViewController.activationDurationDic addEntriesFromDictionary:@{@(durationLabel.tag).stringValue:@(duration)}];
	
	if (self.webViewController.activationDurationDic.count == 2) {
		[self.activateButton setEnabled:YES];
	}
	
    dispatch_async(dispatch_get_main_queue(), ^{
        __weak NSString *durationString = [NSString stringWithFormat:@"%3ld 秒", duration];
        [durationLabel setStringValue:durationString];
    });
}

- (void)turnOffOven:(NSInteger)ovenNumber {
    NSString *filePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"ports.plist"];
	NSArray *ports = [NSArray arrayWithContentsOfFile:filePath];
    
    NSString *offMsg = [NSString stringWithFormat:@"echo %ld > %@", ovenNumber, ports[0]];
    FILE *serialPort = popen([offMsg UTF8String], "r");
    pclose(serialPort);
    NSLog(@"Turn Off Oven: %ld", ovenNumber);
    NSLog(@"Turn Off Msg : %@", offMsg);
}

- (void)updateDurationLabel1 {
    if ([[_oven1ActivationDurationLabel stringValue] integerValue] > 0) {
        NSInteger seconds = _oven1ActivationDurationLabel.stringValue.integerValue - 1;
		self.webViewController.activationDurationDic[@(0).stringValue] = @(seconds);
        _oven1ActivationDurationLabel.stringValue = [NSString stringWithFormat:@"%3ld", seconds];
    } else {
        [self turnOffOven:0];
        NSString *ovenName = _locationList[self.locationTableView.selectedRow][@"location"];
        [self ovenOffAlert:ovenName];
        [self.oven1Timer invalidate];
        [self changeActivateButtonToDefaultState];
        [self.webViewController stopAnimation1];
    }
}

- (void)updateDurationLabel2 {
    if ([[_oven2ActivationDurationLabel stringValue] integerValue] > 0) {
        NSInteger seconds = _oven2ActivationDurationLabel.stringValue.integerValue - 1;
		self.webViewController.activationDurationDic[@(1).stringValue] = @(seconds);
        _oven2ActivationDurationLabel.stringValue = [NSString stringWithFormat:@"%3ld", seconds];
    } else {
        [self turnOffOven:1];
        NSString *ovenName = _locationList[self.locationTableView2.selectedRow][@"location"];
        [self ovenOffAlert:ovenName];
        [self.oven2Timer invalidate];
        [self changeActivateButtonToDefaultState];
        [self.webViewController stopAnimation2];
    }   
}

- (void)changeActivateButtonToDefaultState {
    if (![self.oven1Timer isValid] && ![self.oven2Timer isValid]) {
        [self.activateButton setTitle:@"啟動烤箱"];
        [self.activateButton setAction:@selector(activateOven:)];
		[self.activateButton setEnabled:NO];
		[self toggleAllUI:self.activateButton.isEnabled];
    }
}

- (IBAction)openPortSettingWindow:(id)sender {
    NSLog(@"openPortSettingWindow...");
    [self.settingsViewController.settingsWindow makeKeyAndOrderFront:NSApp];
}

- (void)ovenOffAlert:(NSString *)ovenName {
    
    NSString *messageText = [NSString stringWithFormat:@"%@ 烤好囉！", ovenName];
    NSString *informativeText = [NSString stringWithFormat:@"%@ 烤好囉！請享用 %@ 帶給你的驚喜吧～", ovenName, ovenName];
    NSAlert *alert = [NSAlert alertWithMessageText:messageText defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:informativeText, nil];
   
    CGRect frame = self.window.frame;
    
    frame.origin.x -= 100;
    frame.origin.y -= 100;
    frame.size.height /= 4;
    frame.size.width /= 2;
    [alert beginSheetModalForWindow:[[NSWindow alloc] initWithContentRect:frame styleMask:1 backing:NSBackingStoreNonretained defer:NO] modalDelegate:nil didEndSelector:nil contextInfo:nil];

}

@end


