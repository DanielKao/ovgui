//
//  OVAppDelegate.h
//  OVGUI
//
//  Created by Daniel on 6/19/13.
//  Copyright (c) 2013 Daniel Kao. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "OVWindow.h"

@class OVTrack;
@class OVSettingsViewController;
@class OVWebViewController;

@interface OVAppDelegate : NSObject <NSApplicationDelegate, NSTableViewDataSource, NSTableViewDelegate, NSDatePickerCellDelegate>

@property (assign) IBOutlet OVWindow *window;
@property (weak) IBOutlet NSDatePicker *datePicker;
@property (weak) IBOutlet NSDatePicker *startHourPicker;
@property (weak) IBOutlet NSDatePicker *endHourPicker;
@property (weak) IBOutlet NSTableView *locationTableView;
@property (weak) IBOutlet NSTableView *locationTableView2;
@property (weak) IBOutlet NSTextField *dateRangeLabel;
@property (weak) IBOutlet NSButton *activateButton;

@property (strong) NSMutableArray *locationList;
@property (strong) NSMutableArray *location1UVList;
@property (strong) NSMutableArray *location2UVList;
@property (nonatomic, weak) IBOutlet NSTextField *oven1ActivationDurationLabel;
@property (nonatomic, weak) IBOutlet NSTextField *oven2ActivationDurationLabel;
@property (weak) IBOutlet OVSettingsViewController *settingsViewController;
@property (weak) IBOutlet OVWebViewController *webViewController;

@property (nonatomic, strong) NSMutableArray *activationDurationList;
@property (nonatomic, weak) NSTimer *oven1Timer;
@property (nonatomic, weak) NSTimer *oven2Timer;
@property (nonatomic)       NSInteger finishCounter;
@property (nonatomic, strong) NSMutableArray *windows;

- (IBAction)calculateActivationDuration:(NSButton *)sender;
- (IBAction)activateOven:(id)sender;
- (IBAction)forceStopOven:(id)sender;
- (void)setDurationLabelWithLocation:(NSString *)locationName;
- (void)updateDurationLabel1;
- (void)updateDurationLabel2;
- (IBAction)openPortSettingWindow:(id)sender;
@end
