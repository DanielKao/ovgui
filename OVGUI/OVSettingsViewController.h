//
//  OVSettingsViewController.h
//  OVGUI
//
//  Created by Yu-Cheng Kao on 7/15/13.
//  Copyright (c) 2013 Daniel Kao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OVSettingsViewController : NSObject <NSWindowDelegate>
@property (weak) IBOutlet NSTextField *port1TextField;
@property (weak) IBOutlet NSTextField *port2TextField;
@property (nonatomic, weak) IBOutlet NSWindow *settingsWindow;
- (IBAction)save:(id)sender;

- (void)windowsDidExpose:(NSNotification *)notification;
@end
